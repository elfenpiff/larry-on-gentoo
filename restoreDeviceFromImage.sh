#!/bin/bash

BOOT_FILE=$(pwd)/boot.tar.xz
ROOT_FILE=$(pwd)/root.tar.xz
DEVICE=$1
IMAGE_FILE=$2
USB_BOOT=$3
TAR_ARGS="xpJf"

function help() {
    echo
    echo "restoreDeviceFromImage.sh DEVICE IMAGE_FILE [OPTIONAL]usb-boot"
    echo
    echo usb-boot   - image is able to boot from usb
    echo
    exit
}

function configureDevice() {
    echo "Creating partition on ${DEVICE}"
    (
        echo o      # create a new empty DOS partition table

        echo n      # add a new partition
        echo p      # primary partition
        echo 1      # partition number 1
        echo        # first sector
        echo +256M  # 256mb size
        echo t      # change a partition type
        echo 0c     # W95 FAT32 (LBA)

        echo n      # add a new partition
        echo p      # primary partition
        echo 2      # partition number 2
        echo        # first sector
        echo        # last sector (remainding space)

        echo w      # write and quit
    ) | fdisk ${DEVICE} > /dev/null 2> /dev/null

    partprobe
}

function configureBootLoader() {
    mount ${DEVICE}1 /mnt/boot
    if [[ "$USB_BOOT" == "usb-boot" ]]; then 
        echo "Preparing image to boot from USB"
    else
        echo "Preparing image to boot from SD-Card"
        sed -i 's/\/dev\/sda2/\/dev\/mmcblk0p2/g' /mnt/boot/cmdline.txt
        sed -i 's/\(program_usb_boot_mode=1\)/### \1/g' /mnt/boot/config.txt
        sed -i 's/^\(\/dev\/sda*\)/### \1/g' /mnt/etc/fstab
        sed -i 's/^# \(\/dev\/mmcblk0p*\)/\1/g' /mnt/etc/fstab
    fi

    # disabling hdmi settings
    sed -i 's/\(hdmi*\)/### \1/g' /mnt/boot/config.txt
    umount /mnt/boot

    sync
}

if [[ $(whoami) != "root" ]]; then 
    echo "you must be root to restore an image"
    exit
fi

if [[ -z $1 ]]; then
    echo "First argument has to be the device file where you want to restore the image file"
    echo "to, e.g. /dev/sdc"
    help
fi

if [[ -z $IMAGE_FILE ]]; then
    echo "Second argument has to be the image file"
    help
fi
if [[ ! -f $IMAGE_FILE ]]; then 
    echo "Image file $IMAGE_FILE does not exist"
    help
fi

configureDevice

echo -n "Formatting partitions"
mkfs -t vfat -F 32 ${DEVICE}1
mkfs -i 8192 -t ext4 ${DEVICE}2

echo -n "Extracting $IMAGE_FILE : "
tar xf $IMAGE_FILE
echo "done"

echo -n "Extracting boot image : "
mount ${DEVICE}1 /mnt
cd /mnt 
tar ${TAR_ARGS} $BOOT_FILE --numeric-owner --xattrs-include="*.*"
cd /
sync
umount /mnt
echo "done"

echo -n "Extracting root image : "
mount ${DEVICE}2 /mnt
cd /mnt 
tar ${TAR_ARGS} $ROOT_FILE --numeric-owner --xattrs-include="*.*"
cd /
sync
configureBootLoader
umount /mnt
echo "done"

rm $ROOT_FILE $BOOT_FILE
