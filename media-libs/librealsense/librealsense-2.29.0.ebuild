# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit linux-info toolchain-funcs cmake-utils epatch

inherit git-r3
EGIT_REPO_URI="https://github.com/IntelRealSense/${PN}.git"
EGIT_COMMIT="v${PV}"
SRC_URI="http://realsense-hw-public.s3-eu-west-1.amazonaws.com/Releases/RS4xx/FW/D4XX_FW_Image-5.11.15.0.bin
         http://realsense-hw-public.s3-eu-west-1.amazonaws.com/Releases/RS4xx/FW/D4XX_RC_Image-5.11.15.0.bin
         http://realsense-hw-public.s3-eu-west-1.amazonaws.com/Releases/SR300/FW/SR3XX_FW_Image-3.26.1.0.bin
         http://realsense-hw-public.s3.amazonaws.com/Releases/TM2/FW/target/0.1.0.279/target-0.1.0.279.mvcmd
         http://realsense-hw-public.s3.amazonaws.com/Releases/TM2/FW/app/2.0.19.271/central_app-2.0.19.271.bin
         http://realsense-hw-public.s3.amazonaws.com/Releases/TM2/FW/bl/1.0.1.112/central_bl-1.0.1.112.bin"
KEYWORDS="amd64 ~x86 arm64"

DESCRIPTION="Intel's RealSense 3D Camera API"
HOMEPAGE="https://github.com/IntelRealSense/librealsense"

LICENSE="Apache-2.0"
SLOT="0"
IUSE="+examples"

RDEPEND="
	virtual/libusb:1
	media-libs/libuvc
	examples? ( >=media-libs/glfw-3.3 >=media-libs/opencv-4.1 )
"
DEPEND="${RDEPEND}
	sys-kernel/linux-headers
	virtual/pkgconfig
"

CONFIG_CHECK="USB_VIDEO_CLASS"
ERROR_USB_VIDEO_CLASS="librealsense requires CONFIG_USB_VIDEO_CLASS enabled."

PATCHES=(
	"${FILESDIR}/unix_config.cmake-2.29.0.patch"
	"${FILESDIR}/CMakeLists.txt.patch"
	"${FILESDIR}/CMakeLists.txt-firmware-2.29.0.patch"
	"${FILESDIR}/CMakeLists.txt-thirdparty-2.29.0.patch"
	"${FILESDIR}/connectivity_check.cmake.patch"
)

pkg_pretend() {
	kernel_is ge 4 4 || die "Upstream has deprecated support for kernels < 4.4."

	if tc-is-gcc && [[ gcc-version < 4.9 ]]; then
		die "Upstream requires at least GCC-4.9"
	fi
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_EXAMPLES=$(usex examples true false)
		-DBUILD_GRAPHICAL_EXAMPLES=$(usex examples true false)
		-DBUILD_CV_EXAMPLES=$(usex examples true false)
		-DBUILD_CV_KINFU_EXAMPLE=$(usex examples true false)
		-DFORCE_LIBUVC=true
		-DBUILD_WITH_TM2=true
		-DCMAKE_BUILD_TYPE=release
	)

	cmake-utils_src_configure

	BUILD_DIR=${WORKDIR}/${P}_build
	mkdir -p ${BUILD_DIR}/common/fw
	mkdir -p ${BUILD_DIR}/third-party/libtm/fw
	cp ${DISTDIR}/D4XX_FW_Image-5.11.15.0.bin ${BUILD_DIR}/common/fw/
	cp ${DISTDIR}/D4XX_RC_Image-5.11.15.0.bin ${BUILD_DIR}/common/fw/
	cp ${DISTDIR}/SR3XX_FW_Image-3.26.1.0.bin ${BUILD_DIR}/common/fw/
	cp ${DISTDIR}/target-0.1.0.279.mvcmd ${BUILD_DIR}/third-party/libtm/fw/
	cp ${DISTDIR}/central_app-2.0.19.271.bin ${BUILD_DIR}/third-party/libtm/fw
	cp ${DISTDIR}/central_bl-1.0.1.112.bin ${BUILD_DIR}/third-party/libtm/fw
}

src_compile() {
	pushd "${BUILD_DIR}" &>/dev/null
	emake
	popd &>/dev/null
}

src_install() {
	einstalldocs

	dolib "${BUILD_DIR}"/librealsense2.so*

	insinto /usr/include/
	doins -r include/librealsense2

	insinto /lib/udev/rules.d/
	doins config/99-realsense-libusb.rules

	insinto /usr/share/${PF}
	doins scripts/realsense-camera-formats.patch

	exeinto /usr/bin/
	find "${BUILD_DIR}/tools" -type f -executable -exec doexe '{}' +

	if use examples; then
		exeinto /usr/share/${PF}/examples/
		find "${BUILD_DIR}/examples/" -type f -executable -exec doexe '{}' +

		exeinto /usr/share/${PF}/examples/opencv/
		find "${BUILD_DIR}/wrappers/opencv" -type f -executable -exec doexe '{}' +
	fi
}

pkg_postinst() {
	ewarn "All examples can be found under /usr/share/${PF}/examples/"
}
